from recurs import rec


def data_input():
    """
    Функция для ввода данных

    Вводится количество ребер и вершин, наличие связей

    :return: mas, количество ребер, вершин, наличие связей
    :rtype: List[int, int, List[Tuple(int, int)]]
    """
    flag = 1
    r = v = 0
    while flag != 0:
        v = input('Введите число вершин > 4 = ')
        if v.isdigit() and int(v) > 4:
            flag = 0
        else:
            print('Некорректный ввод!')
    flag = 1
    v = int(v)
    while flag != 0:
        r = input('Введите число ребер > 3 = ')
        if r.isdigit() and 3 < int(r) <= rec(v):
            flag = 0
        else:
            print('Некорректный ввод!')
    r = int(r)
    p = []
    k = 0
    while k != r:
        try:
            i, j = (map(int, input('Введите два числа "0 < число < v" = ').split()))
            numbers = (i, j)
            if (0 < i <= v) and (0 < j <= v) and (i != j) and not (numbers in p) and not (numbers[::-1] in p):
                p.append(numbers)
                k += 1
            else:
                print('Некорректный ввод!')
        except ValueError:
            print('Некорректный ввод!')
    mas = [v, r, p]
    return mas
