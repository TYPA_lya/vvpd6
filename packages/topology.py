from .full import full


def topology(x, y, links):
    """
    Функция определяет тип связи

    :param x: количество вершин
    :type x: int
    :param y: количество ребер
    :type y: int
    :param links: Наличие связей между вершинами
    :type links: List[Tuple(int, int)]
    :return: Тип связи
    :rtype: str
    """
    if full(x, y):
        return 'Полносвязная'
    for num in range(1, x + 1):
        n = 0
        for tup in links:
            if num in tup:
                n += 1
        if n == len(links):
            return 'Неполносвязная - Звезда'
    two = one = 0
    a = [0] * (x + 1)
    for tup in links:
        for num in tup:
            a[num] += 1
    for num in a:
        if num == 2:
            two += 2
        elif num == 1:
            one += 1
    if (two + one == len(links) * 2) and one == 2:
        return 'Неполносвязная - Шина'
    if two == len(links) * 2:
        return 'Неполносвязная - Кольцо'
    return 'Неполносвязная'
