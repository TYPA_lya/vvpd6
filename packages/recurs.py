def rec(x):
    """
    Рекурсивная функция, считает количество связей

    :param x: число вершин
    :type x: int
    :return: n, количество связей
    :rtype: int
    """
    n = 0
    if x == 5:
        return 10
    else:
        n += (rec(x - 1) + (x - 1))
    return n
