from .recurs import rec


def full(x, y):
    """
    Функция проверки на полноценность связи

    :param x: Количество вершин
    :type x: int
    :param y: Количество ребер
    :type y: int
    :return: Является ли тип связи полносвязным
    :rtype: Bool
    """
    if rec(x) == y:
        return True
    return False
