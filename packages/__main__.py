from data_input import data_input
from topology import topology


def reenter():
    """
    Функция повторного ввода или завершения
    """
    while (s := input('Для завершения введите stop: ')) != 'stop':
        data = data_input()
        result = topology(*data)
        print(result)


if __name__ == '__main__':
    reenter()
