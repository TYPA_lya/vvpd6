import pytest


@pytest.fixture()
def data():
    return [6, 6, [(10, 9), (9, 5), (5, 6), (6, 7), (7, 8), (8, 10)], IndexError]
