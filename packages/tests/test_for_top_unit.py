import unittest
from ..topology import topology


class TestTopology(unittest.TestCase):
    def test_topology_good(self):
        self.assertEqual(topology(5, 10, [(1, 2), (1, 3), (1, 4), (1, 5), (2, 3),
                                          (2, 4), (2, 5), (3, 4), (3, 5), (4, 5)]), 'Полносвязная')
        self.assertEqual(topology(6, 7, [(1, 2), (2, 3), (1, 3), (4, 3),
                                         (5, 4), (3, 5), (1, 4)]), 'Неполносвязная')
        self.assertEqual(topology(6, 5, [(1, 2), (2, 3), (3, 4), (4, 5), (5, 6)]), 'Неполносвязная - Шина')
        self.assertEqual(topology(5, 4, [(1, 2), (1, 3), (1, 4), (1, 5)]), 'Неполносвязная - Звезда')
        self.assertEqual(topology(6, 6, [(1, 2), (2, 3), (3, 4),
                                         (4, 5), (5, 6), (6, 1)]), 'Неполносвязная - Кольцо')
