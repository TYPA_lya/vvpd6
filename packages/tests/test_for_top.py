import pytest
from packages.topology import topology

data_good = [(5, 10, [(1, 2), (1, 3), (1, 4), (1, 5), (2, 3), (2, 4), (2, 5), (3, 4), (3, 5), (4, 5)], 'Полносвязная'),
             (6, 7, [(1, 2), (2, 3), (1, 3), (4, 3), (5, 4), (3, 5), (1, 4)], 'Неполносвязная'),
             (6, 5, [(1, 2), (2, 3), (3, 4), (4, 5), (5, 6)], 'Неполносвязная - Шина'),
             (5, 4, [(1, 2), (1, 3), (1, 4), (1, 5)], 'Неполносвязная - Звезда'),
             (6, 6, [(1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 1)], 'Неполносвязная - Кольцо')]


@pytest.mark.parametrize("tops, ribs, numbers, expected_result", data_good)
def test_topology_good(tops, ribs, numbers, expected_result):
    assert topology(tops, ribs, numbers) == expected_result


def test_topology_with_error():
    with pytest.raises(TypeError):
        topology(6, 6, [('1', 2), (2, 3), (3, 4), (4, '5'), (5, 6), (6, 1)])


def test_topology_with_fixture(data):
    with pytest.raises(data[-1]):
        topology(data[0], data[1], data[2])
